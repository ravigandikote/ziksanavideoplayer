package vo
{
	public class VideoVO
	{
		public var title:String;
		public var thumbnailSrc:String;
		public var source:String;
		public var tags:Array = new Array();
		public var notes:Array = new Array();
	}
}