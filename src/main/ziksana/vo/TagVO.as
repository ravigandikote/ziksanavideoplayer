package vo
{
	public class TagVO
	{
		public var title:String;
		public var description:String;
		public var inTime:String;
		public var outTime:String;
		public var xpos:String;
		public var ypos:String;
	}
}