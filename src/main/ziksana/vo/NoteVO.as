package vo
{
	public class NoteVO
	{
		public var noteId:int;
		public var title:String;
		public var description:String;
	}
}