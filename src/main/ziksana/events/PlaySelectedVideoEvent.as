package events
{
	import flash.events.Event;
	
	import vo.VideoVO;
	
	public class PlaySelectedVideoEvent extends Event
	{
		public static const PLAY_VIDEO:String = "playVideo";
		private var _videoVO:VideoVO;
		
		public function PlaySelectedVideoEvent(type:String, video:VideoVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_videoVO = video;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new PlaySelectedVideoEvent(type, videoVO, bubbles, cancelable);
		}
		
		public function get videoVO():VideoVO
		{
			return _videoVO;
		}
	}
}