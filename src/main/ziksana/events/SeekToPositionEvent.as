package events
{
	import flash.events.Event;
	
	import vo.VideoVO;
	
	public class SeekToPositionEvent extends Event
	{
		public static const SEEK_TO_POS:String = "seekToPos";
		private var _seekToPos:Number;
		
		public function SeekToPositionEvent(type:String, seekToPos:Number, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_seekToPos = seekToPos;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new SeekToPositionEvent(type, seekToPos, bubbles, cancelable);
		}
		
		public function get seekToPos():Number
		{
			return _seekToPos;
		}
	}
}