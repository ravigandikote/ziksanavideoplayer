package events
{
	import flash.events.Event;
	
	public class FullScreenEvent extends Event
	{
		public static const FULL_SCREEN:String = "fullScreen";
		
		public function FullScreenEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new FullScreenEvent(type, bubbles, cancelable);
		}
	}
}