package events
{
	import flash.events.Event;
	
	import vo.NoteVO;
	import vo.VideoVO;
	
	public class UpdateNoteEvent extends Event
	{
		public static const UPDATE_NOTE:String = "updateNote";
		private var _note:NoteVO;
		
		public function UpdateNoteEvent(type:String, note:NoteVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_note = note;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new UpdateNoteEvent(type, note, bubbles, cancelable);
		}
		
		public function get note():NoteVO
		{
			return _note;
		}
	}
}