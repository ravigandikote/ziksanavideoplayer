package events
{
	import flash.events.Event;
	
	public class ReadyForAddingTipEvent extends Event
	{
		public static const READY_FOR_ADDING_TIP:String = "readyForAddingTip";
		public static const GOT_POSITION_TO_ADD_TIP:String = "gotPositionToAddTip";
		
		private var _positions:Object;
		
		public function ReadyForAddingTipEvent(type:String, positions:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_positions = positions;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ReadyForAddingTipEvent(type, positions, bubbles, cancelable);
		}
		
		public function get positions():Object
		{
			return _positions;
		}
	}
}