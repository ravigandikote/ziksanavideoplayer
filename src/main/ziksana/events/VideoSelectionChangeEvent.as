package events
{
	import flash.events.Event;
	
	import vo.VideoVO;
	
	public class VideoSelectionChangeEvent extends Event
	{
		public static const VIDEO_SELECTED:String = "videoSelected";
		protected var _selectedVideo:VideoVO;
		
		public function VideoSelectionChangeEvent(type:String, selectedVideo:VideoVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_selectedVideo = selectedVideo;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new VideoSelectionChangeEvent(type, selectedVideo, bubbles, cancelable);
		}
		
		public function get selectedVideo():VideoVO
		{
			return _selectedVideo;
		}
	}
}