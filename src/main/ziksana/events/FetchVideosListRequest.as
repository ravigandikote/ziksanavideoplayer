package events
{
	import flash.events.Event;
	
	public class FetchVideosListRequest extends Event
	{
		public static const FETCH_VIDEOS_LIST:String = "FetchVideosList";
		
		public function FetchVideosListRequest()
		{
			super(FETCH_VIDEOS_LIST);
		}
	}
}