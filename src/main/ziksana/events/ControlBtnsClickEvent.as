package events
{
	import flash.events.Event;
	
	public class ControlBtnsClickEvent extends Event
	{
		public static const CONTROL_BTNS_CLICK:String = "controlBtnsClick";
		private var _itemClicked:String;
		
		public function ControlBtnsClickEvent(type:String, itemClicked:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_itemClicked = itemClicked;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ControlBtnsClickEvent(type, itemClicked, bubbles, cancelable);
		}
		
		public function get itemClicked():String
		{
			return _itemClicked;
		}
	}
}