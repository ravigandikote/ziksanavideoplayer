package events
{
	import flash.events.Event;
	
	public class HideModalDialogEvent extends Event
	{
		public static const HIDE_DIALOG:String = "hideDialog";
		public static const HIDE_ADD_TIP_VIEW:String = "hideAddTipView";
		public static const HIDE_DYNAMIC_TIP_VIEW:String = "hideDynamicTipView";
		
		public function HideModalDialogEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new HideModalDialogEvent(type, bubbles, cancelable);
		}
	}
}