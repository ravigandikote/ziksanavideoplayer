package events
{
	import flash.events.Event;
	
	import vo.VideoVO;
	
	public class UpdatePlayheadPositionEvent extends Event
	{
		public static const UPDATE_PLAYHEAD_POSITION:String = "updatePlayheadPosition";
		private var _videoMetaData:Object;
		
		public function UpdatePlayheadPositionEvent(type:String, videoMetaData:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_videoMetaData = videoMetaData;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new UpdatePlayheadPositionEvent(type, videoMetaData, bubbles, cancelable);
		}
		
		public function get videoMetaData():Object
		{
			return _videoMetaData;
		}
	}
}