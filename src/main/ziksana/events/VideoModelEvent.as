package events
{
	import flash.events.Event;
	
	public class VideoModelEvent extends Event
	{
		public static const DATA_UPDATED:String = "dataUpdated";
		protected var _videos:Array;
		
		public function VideoModelEvent(type:String, videos:Array, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_videos = videos;	
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new VideoModelEvent(type, videos, bubbles, cancelable);
		}
		
		public function get videos():Array
		{
			return _videos;
		}
	}
}