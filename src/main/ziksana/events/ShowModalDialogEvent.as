package events
{
	import flash.events.Event;
	
	import vo.TagVO;
	
	public class ShowModalDialogEvent extends Event
	{
		public static const SHOW_DIALOG:String = "showDialog";
		public static const SHOW_NOTE_VIEW:String = "showNoteView";
		public static const SHOW_ADD_TIP_VIEW:String = "showAddTipView";
		public static const SHOW_TIP_VIEW:String = "showTipView";
		
		private var _tip:TagVO;
		
		public function ShowModalDialogEvent(type:String, tip:TagVO, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_tip = tip;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ShowModalDialogEvent(type, tip, bubbles, cancelable);
		}
		
		public function get tip():TagVO
		{
			return _tip;
		}
	}
}