package
{
	import commands.FetchVideosListCommand;
	
	import events.FetchVideosListRequest;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	import mediators.AppMediator;
	import mediators.ModalDialogMediator;
	import mediators.NotesDisplayMediator;
	import mediators.SideButtonsMediator;
	import mediators.VideoControlsMediator;
	import mediators.VideoDisplayMediator;
	import mediators.VideosListMediator;
	
	import models.AppModel;
	import models.VideoModel;
	
	import org.robotlegs.core.IContext;
	import org.robotlegs.core.IInjector;
	import org.robotlegs.mvcs.Context;
	
	import services.VideoService;
	
	import views.AppView;
	import views.ModalDialog;
	import views.NotesDisplay;
	import views.SideButtonsView;
	import views.VideoControls;
	import views.VideoDisplay;
	import views.VideosList;

	public class ZiksanaContext extends Context
	{	
		private static var contextDispatcher:IEventDispatcher;
		private static var contextInjector:IInjector;
		private var context:DisplayObjectContainer;
		
		public function ZiksanaContext(contextView:DisplayObjectContainer)
		{
			super(contextView);
			this.context = contextView;
			contextDispatcher = eventDispatcher;
			contextInjector = injector;
		}
		
		override public function startup():void
		{
			//Mapping Singletons (models and services)
			injector.mapSingleton(AppModel);
			injector.mapSingleton(VideoService);
			injector.mapSingleton(VideoModel);
			
			//Commands Mapping here
			commandMap.mapEvent(FetchVideosListRequest.FETCH_VIDEOS_LIST,FetchVideosListCommand);
			
			//Mediators Mapping here
			mediatorMap.mapView(VideoDisplay,VideoDisplayMediator);
			mediatorMap.mapView(VideosList,VideosListMediator);
			mediatorMap.mapView(VideoControls,VideoControlsMediator);
			mediatorMap.mapView(ModalDialog,ModalDialogMediator);
			mediatorMap.mapView(SideButtonsView,SideButtonsMediator);
			mediatorMap.mapView(NotesDisplay,NotesDisplayMediator);
			mediatorMap.mapView(AppView,AppMediator);
			
			super.startup();
		}
		
		public static function dispatch(event:Event):void
		{
			contextDispatcher.dispatchEvent(event);
		}
		
		public static function injectInto(target:Object):void
		{
			contextInjector.injectInto(target);	
		}
	}
}