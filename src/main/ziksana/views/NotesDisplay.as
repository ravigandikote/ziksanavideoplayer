package views
{
	import com.soma.ui.layouts.CanvasUI;
	import com.soma.ui.layouts.VBoxUI;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class NotesDisplay extends Sprite
	{
		public var notesCanv:CanvasUI;
		
		public function NotesDisplay()
		{
			super();
			initLayout();
		}
		
		private function initLayout():void
		{
			notesCanv = new CanvasUI(this,360,120);
			notesCanv.backgroundAlpha = 1;
			notesCanv.backgroundColor = 0x000000;
			
			notesCanv.scrollRect = new Rectangle(notesCanv.x,notesCanv.y,360,80);
			this.addChild(notesCanv);
		}
	}
}