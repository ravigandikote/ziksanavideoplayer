package views
{
	import com.soma.ui.ElementUI;
	import com.soma.ui.layouts.CanvasUI;
	import com.soma.ui.layouts.HBoxUI;
	
	import events.ShowModalDialogEvent;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.*;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import models.AppModel;
	
	import vo.TagVO;
	
	public class ModalDialog extends Sprite
	{
		public var titleTxt:TextField;
		public var descTxt:TextField;
		
		public var titleTxtFormat:TextFormat;
		public var descTxtFormat:TextFormat;
		
		public var dialogWidth:Number;
		public var dialogHeight:Number;
		
		public var canvas:CanvasUI;
		public var sprite:CanvasUI;
		
		public var closeBtn:Sprite;
		public var closeLbl:TextField;
		public var closeBtnFormat:TextFormat;
		
		private var editable:Boolean;
		
		public var okBtn:CanvasUI;
		public var errorTxt:TextField;
		public var dialogType:String;
		public var tipVO:TagVO;
		
		public function ModalDialog(type:String,tip:TagVO):void
		{
			titleTxt = new TextField();
			descTxt = new TextField();
			tipVO = tip as TagVO;
			dialogType = type;
			switch(dialogType)
			{
				case ShowModalDialogEvent.SHOW_NOTE_VIEW:
				case ShowModalDialogEvent.SHOW_ADD_TIP_VIEW:
					dialogWidth = 130;
					dialogHeight = 110;
					drawContainers();
					getEditView(tip);
					break;

				case ShowModalDialogEvent.SHOW_TIP_VIEW:
					dialogWidth = 100;
					dialogHeight = 50;
					drawContainers();
					getTipView(tip);
					break;
			}
		}
		
		private function drawContainers():void
		{
			/* outer canvas */
			canvas = new CanvasUI(this, dialogWidth, dialogHeight);
			canvas.backgroundColor = 0xFFFFFF;
			canvas.backgroundAlpha = 0.8;
			this.addChild(canvas);
			
			/* Inner container */
			sprite = new CanvasUI(this, dialogWidth-20, dialogHeight-20);
			sprite.backgroundAlpha = 1;
			sprite.backgroundColor = 0x000000;
			sprite.x = sprite.y = 10;
			
			/* close button */
			closeBtn = new Sprite();
			closeLbl = new TextField();
			closeLbl.text = "Close";
			closeBtn.x = (dialogWidth-20);
			closeBtn.buttonMode = closeBtn.useHandCursor = true;
			
			closeBtn.addChild(closeLbl);
			closeBtn.mouseChildren = false;
			
			closeBtnFormat = new TextFormat();
			closeBtnFormat.size = 6;
			closeBtnFormat.underline = true;
			closeBtnFormat.italic = true;
			
			closeLbl.setTextFormat(closeBtnFormat);
			
			canvas.addChild(sprite);
			canvas.addChild(closeBtn);
		}
		
		private function getTipView(tip:TagVO):void
		{
			var tipTitle:TextField = new TextField();
			tipTitle.text = tip.title;
			tipTitle.gridFitType = GridFitType.SUBPIXEL;
			var tipTitleFormat:TextFormat = new TextFormat();
			tipTitleFormat.color = 0xFF9900;
			tipTitleFormat.size = 10;
			tipTitle.setTextFormat(tipTitleFormat);

			var tipDesc:TextField = new TextField();
			tipDesc.text = tip.description;
			tipDesc.gridFitType = GridFitType.SUBPIXEL;
			var tipDescFormat:TextFormat = new TextFormat();
			tipDescFormat.color = 0xFFFFFF;
			tipDescFormat.size = 6;
			tipDesc.y = 15;
			tipDesc.setTextFormat(tipDescFormat);
			
			sprite.addChild(tipTitle);
			sprite.addChild(tipDesc);
		}
		
		private function getEditView(tip:TagVO):void
		{
			/* Title */
			var titleTxtCanv:CanvasUI = new CanvasUI(sprite,80,15);
			var titleLbl:TextField = new TextField();
			titleLbl.text = "Title : ";
			titleLbl.width = 20;
			var titleLblFormat:TextFormat = new TextFormat();
			titleLblFormat.color = 0xFF9900;
			titleLblFormat.size = 7;
			titleLbl.setTextFormat(titleLblFormat);
			titleTxtCanv.backgroundAlpha = 1;
			titleTxtCanv.backgroundColor = 0xFFFFFF;
			
			titleTxtFormat = new TextFormat();
			titleTxtFormat.size = 7;
			titleTxtFormat.color = 0xFF9900;
			
			titleTxt.setTextFormat(titleTxtFormat);
			titleTxt.width = 65;
			titleTxt.height = 15;
			titleTxtCanv.addChild(titleTxt);
			titleTxtCanv.addChild(titleLbl);
			titleTxt.x = 15;
			titleTxtCanv.x = titleTxtCanv.y = 10;
			
			/* Description */
			var descTxtCanv:CanvasUI = new CanvasUI(sprite, 80, 25);
			var descLbl:TextField = new TextField();
			var descLblFormat:TextFormat = new TextFormat();
			descLblFormat.color = 0xFF9900;
			descLblFormat.size = 7;
			descLbl.text = "Description : ";
			descLbl.width = 40;
			descLbl.setTextFormat(descLblFormat);
			descTxtCanv.backgroundAlpha = 1;
			descTxtCanv.backgroundColor = 0xFFFFFF;
			
			descTxtFormat = new TextFormat();
			descTxtFormat.size = 7;
			descTxtFormat.color = 0xFFFFFF;
			
			descTxt.setTextFormat(descTxtFormat);
			descTxt.multiline = true;
			descTxt.wordWrap = true;
			descTxt.width = 50;
			descTxt.height = 30;
			descTxtCanv.addChild(descTxt);
			descTxtCanv.addChild(descLbl);
			descTxt.x = 30;
			descTxtCanv.y = 30;
			descTxtCanv.x = 10;
			
			/* Ok Button */
			okBtn = new CanvasUI(sprite,15,10);
			okBtn.backgroundAlpha = 1;
			okBtn.backgroundColor = 0xFF9878;
			
			var okBtnLbl:TextField = new TextField();
			okBtnLbl.width = 25;
			okBtnLbl.height = 15;
			okBtnLbl.text = "OK";
			var okBtnTxtFormat:TextFormat = new TextFormat();
			okBtnTxtFormat.size = 7;
			okBtnLbl.setTextFormat(okBtnTxtFormat);
			okBtn.addChild(okBtnLbl);
			okBtn.x = 80;
			okBtn.y = descTxtCanv.y + descTxtCanv.height + 10;
			okBtn.mouseChildren = false;
			okBtn.buttonMode = okBtn.useHandCursor = true;
			
			/* Error Text */
			errorTxt = new TextField();
			errorTxt.x = 5;
			errorTxt.type = "dynamic";
			errorTxt.y = descTxtCanv.y + descTxtCanv.height + 10;
			errorTxt.visible = false;
			
			/* Adding above things to the inner container and close button to the outer canvas */
			sprite.addChild(titleTxtCanv);
			sprite.addChild(descTxtCanv);
			sprite.addChild(okBtn);
			sprite.addChild(errorTxt);
			
			canvas.add(sprite);
			
			descTxt.type = titleTxt.type = "input";
			canvas.refresh();
		}
	}
}