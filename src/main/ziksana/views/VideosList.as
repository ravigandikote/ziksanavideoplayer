package views
{
	import com.soma.ui.layouts.CanvasUI;
	
	import flash.display.Sprite;
	
	public class VideosList extends Sprite
	{
		public var videosHolder:CanvasUI;
		
		public function VideosList()
		{
			super();
			initLayout();
		}
		
		private function initLayout():void
		{
			videosHolder = new CanvasUI(this,60,220);
			videosHolder.backgroundColor = 0x000000;
			videosHolder.backgroundAlpha = 1;
			
			this.addChild(videosHolder);
		}
	}
}