package views
{
	import com.soma.ui.layouts.CanvasUI;
	import com.soma.ui.layouts.HBoxUI;
	
	import flash.display.Sprite;
	import flash.events.*;
	import flash.media.StageVideo;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.text.TextField;
	
	import org.osmf.elements.VideoElement;
	import org.osmf.media.MediaElement;
	import org.osmf.media.MediaPlayer;
	
	public class VideoDisplay extends Sprite
	{
		private var _videoHolder:CanvasUI;
		public var videoURL:String = "assets/videos/gulfoss.flv";
		private var connection:NetConnection;
		public var stream:NetStream;
		public var isPlaying:Boolean = true;
		public var videoDuration:Number;		
		
		public function VideoDisplay()
		{
			super();
			initLayout();
		}
		
		private function netStatusHandler(event:NetStatusEvent):void {
			switch (event.info.code) {
				case "NetConnection.Connect.Success":
					connectStream();
					break;
				case "NetStream.Play.StreamNotFound":
					trace("Unable to locate video: " + videoURL);
					break;
			}
		}
		
		private function connectStream():void {
			stream = new NetStream(connection);
			stream.client = this;
			stream.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			stream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			var video:Video = new Video(300,200);
			video.attachNetStream(stream);
			stream.play(videoURL);
			_videoHolder.addChild(video);
		}
		
		public function onMetaData(e:Object):void
		{
			videoDuration = e.duration;
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("securityErrorHandler: " + event);
		}
		
		private function asyncErrorHandler(event:AsyncErrorEvent):void {
			// ignore AsyncErrorEvent events.
		}
		
		private function initLayout():void
		{
			_videoHolder = new CanvasUI(this,300,200);
			_videoHolder.backgroundColor = 0x000000;
			_videoHolder.backgroundAlpha = 1;
			
			connection = new NetConnection();
			connection.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			connection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			connection.connect(null);
			
			this.addChild(_videoHolder);
		}
		
		public function get videoHolder():CanvasUI
		{
			return _videoHolder as CanvasUI;
		}
	}
}