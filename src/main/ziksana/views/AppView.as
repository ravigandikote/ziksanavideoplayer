package views
{
	import com.soma.ui.layouts.CanvasUI;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	
	public class AppView extends Sprite
	{
		public var mainCanv:CanvasUI;
		public var sideButtons:SideButtonsView;
		public var transCanv:CanvasUI;
		public var videosList:VideosList;
		public var videoDisplay:VideoDisplay;
		public var notesDisplay:NotesDisplay;
		public var _stage:Stage;
		public var _app:ZiksanaVideoPlayer;
		public var controls:VideoControls;
		
		public function AppView(app:ZiksanaVideoPlayer)
		{
			_stage = app.stage;
			_app = app;
			super();
			initLayout();
		}
		
		public function initLayout():void
		{
			mainCanv = new CanvasUI(_app,1000,_stage.height);
			mainCanv.backgroundAlpha = 1;
			mainCanv.backgroundColor = 0xFF9900;
			mainCanv.x = 65;
			/* Videos List */
			videosList = new VideosList();
			videosList.x = videosList.y = 0;
			
			/* Video Display */
			videoDisplay = new VideoDisplay();
			videoDisplay.x = videosList.width;
			videoDisplay.y = 0;
			
			/* Video Controls */
			controls = new VideoControls();
			controls.x = videoDisplay.x;
			controls.y = videoDisplay.height;
			
			/* tags and Note Btns canvas */
			sideButtons = new SideButtonsView();
			sideButtons.x = videoDisplay.width - sideButtons.width + 50;
			sideButtons.y = controls.y + 25 ;
			
			/* Notes Display below the Video part */
			notesDisplay = new NotesDisplay();
			notesDisplay.x = 0;
			notesDisplay.y = videosList.height + 24;
			
			/* Transparent view to give black box effect */
			transCanv = new CanvasUI(mainCanv,videosList.width+videoDisplay.width,videoDisplay.height+notesDisplay.height+30);
			transCanv.backgroundAlpha = 0.9;
			transCanv.backgroundColor = 0x000000;
			transCanv.x = transCanv.y = 0;
			
			/* Main Holder adding all these above views */
			mainCanv.addChild(videosList);
			mainCanv.addChild(sideButtons);
			mainCanv.addChild(notesDisplay);
			mainCanv.addChild(videoDisplay);
			mainCanv.addChild(controls);
			
			this.addChild(mainCanv);
		}
	}
}