package views
{
	import com.soma.ui.layouts.CanvasUI;
	import com.soma.ui.layouts.HBoxUI;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class VideoControls extends Sprite
	{
		public var controlsCanv:CanvasUI;
		
		/* Play Button */
		public var playArea:HBoxUI;
		public var playLbl:TextField;
		
		/* Stop Button */
		public var stopLbl:TextField;
		public var stopArea:HBoxUI;
		
		/* Video Time */
		public var timeTxt:TextField;
		public var timeArea:CanvasUI
		
		/* Seek Bar */
		public var seekCanv:CanvasUI;
		public var seekBarSprite:Sprite;
		public var seekBarHandleSprite:Sprite;
		
		/* Full Screen */
		public var fullScreenBtn:CanvasUI;
		public var fullScreenLbl:TextField;
		
		public function VideoControls()
		{
			super();
			initLayout();
		}
		
		private function initLayout():void
		{
			controlsCanv = new CanvasUI(this,300,20);
			controlsCanv.backgroundColor = 0x000000;
			controlsCanv.backgroundAlpha = 1;
			
			drawPlayBtn();
			drawStopBtn();
			renderVideoTime();
			renderSeekBar();
			renderFullScreenBtn();
			
			this.addChild(controlsCanv);
		}
		
		protected function drawPlayBtn():void
		{
			playArea = new HBoxUI(controlsCanv,25,15);
			playArea.backgroundColor = 0xFF9878;
			playArea.backgroundAlpha = 1;
			playArea.x = 2;
			playArea.y = controlsCanv.height/2 - (playArea.height/2);
			playLbl = new TextField();
			playLbl.width = 25;
			playLbl.height = 15;
			playLbl.text = "Pause";
			playLbl.mouseEnabled = false;
			playArea.addChild(playLbl);
			controlsCanv.addChild(playArea);			
		}
		
		protected function drawStopBtn():void
		{
			stopArea = new HBoxUI(controlsCanv,25,15);
			stopArea.backgroundColor = 0xFF9878;
			stopArea.backgroundAlpha = 1;
			stopArea.x = 2 + playArea.width + 10;
			stopArea.y = controlsCanv.height/2 - (stopArea.height/2);
			stopLbl = new TextField();
			stopLbl.width = 25;
			stopLbl.height = 15;
			stopLbl.text = "Stop";
			stopLbl.mouseEnabled = false;
			stopArea.addChild(stopLbl);
			controlsCanv.addChild(stopArea);
		}
		
		private function renderVideoTime():void
		{
			timeArea = new CanvasUI(controlsCanv,25,15)
			timeArea.backgroundAlpha = 1;
			timeArea.backgroundColor = 0xFFFFFF;
			timeTxt = new TextField();
			timeTxt.width = 25;
			timeTxt.height = 15;
			timeArea.x = 2 + playArea.width + stopArea.width + 20;
			timeArea.y = controlsCanv.height/2 - (stopArea.height/2);
			var timeFormat:TextFormat = new TextFormat();
			timeFormat.color = 0xFF0000;
			timeTxt.setTextFormat(timeFormat);
			timeArea.addChild(timeTxt);
			controlsCanv.addChild(timeArea);
		}
		
		private function renderSeekBar():void
		{
			seekCanv = new CanvasUI(controlsCanv,150,15);
			
			seekBarSprite = new Sprite();
			seekBarSprite.graphics.beginFill(0xFFFFFF, 1);
			seekBarSprite.graphics.drawRect(0, seekCanv.height/2 + 2 , 150, 1);

			seekBarHandleSprite = new Sprite();
			seekBarHandleSprite.graphics.beginFill(0xFFFFFF, 1);
			seekBarHandleSprite.graphics.drawRect(0, seekCanv.height/2 - 1 , 3, 7);
			
			seekCanv.addChild(seekBarSprite);
			seekCanv.addChild(seekBarHandleSprite);
			seekCanv.x = 2 + playArea.width + stopArea.width + timeArea.width + 30;
			
			controlsCanv.addChild(seekCanv);
		}
		
		private function renderFullScreenBtn():void
		{
			fullScreenBtn = new CanvasUI(controlsCanv,20,15);
			fullScreenBtn.x = 2 + playArea.width + stopArea.width + timeArea.width + 30 + seekCanv.width + 10;
			fullScreenBtn.backgroundColor = 0xFF9878;
			fullScreenBtn.backgroundAlpha = 1;
			fullScreenBtn.y = controlsCanv.height/2 - (fullScreenBtn.height/2);
			fullScreenLbl = new TextField();
			fullScreenLbl.width = 20;
			fullScreenLbl.height = 15;
			fullScreenLbl.text = "FS";
			fullScreenLbl.mouseEnabled = false;
			fullScreenBtn.addChild(fullScreenLbl);
			controlsCanv.addChild(fullScreenBtn);
		}
	}
}