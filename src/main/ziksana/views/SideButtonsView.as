package views
{
	import com.soma.ui.layouts.CanvasUI;
	import com.soma.ui.layouts.HBoxUI;
	
	import flash.display.Sprite;
	import flash.text.TextField;
	
	public class SideButtonsView extends Sprite
	{
		public var tagNoteBtnsCanv:CanvasUI;
		public var noteBtn:HBoxUI;
		public var noteLbl:TextField;
		
		public var tagBtn:HBoxUI;
		public var tagLbl:TextField;
		
		public function SideButtonsView()
		{
			super();
			
			initLayout();
		}
		
		private function initLayout():void
		{
			/* tags and Note Btns canvas */
			this.tagNoteBtnsCanv = new CanvasUI(this,120,40);
			this.tagNoteBtnsCanv.backgroundAlpha = 1;
			this.tagNoteBtnsCanv.backgroundColor = 0x000000;
			
			this.addChild(this.tagNoteBtnsCanv);
			
			/* Adding Note and tag buttons */
			drawNoteBtn();
			drawtagBtn();
		}
		
		protected function drawNoteBtn():void
		{
			this.noteBtn = new HBoxUI(this,55,15);
			this.noteBtn.backgroundColor = 0xFF9878;
			this.noteBtn.backgroundAlpha = 1;
			this.noteBtn.x = this.tagNoteBtnsCanv.x +2;
			this.noteBtn.y = this.tagNoteBtnsCanv.y +2;
			
			this.noteLbl = new TextField();
			this.noteLbl.text = "Add Note";
			this.noteLbl.mouseEnabled = false;
			this.noteBtn.addChild(this.noteLbl);
			
			this.tagNoteBtnsCanv.addChild(this.noteBtn);	
		}
		
		protected function drawtagBtn():void
		{
			this.tagBtn = new HBoxUI(this,55,15);
			this.tagBtn.backgroundColor = 0xFF9878;
			this.tagBtn.backgroundAlpha = 1;
			this.tagBtn.x = this.tagNoteBtnsCanv.x + this.noteBtn.width + 8;
			this.tagBtn.y = this.tagNoteBtnsCanv.y + 2;
			
			this.tagLbl = new TextField();
			this.tagLbl.text = "Add Tip";
			this.tagLbl.mouseEnabled = false;
			this.tagBtn.addChild(this.tagLbl);
			
			this.tagNoteBtnsCanv.addChild(this.tagBtn);	
		}
	}
}