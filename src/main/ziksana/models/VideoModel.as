package models
{
	import events.SeekToPositionEvent;
	import events.VideoModelEvent;
	import events.VideoSelectionChangeEvent;
	
	import org.robotlegs.mvcs.Actor;
	
	import vo.VideoVO;
	
	public class VideoModel extends Actor
	{
		private var _videosArr:Array = new Array();
		private var _selectedVideo:VideoVO;
		private var _seekToPos:Number;
		
		public function get videosArr():Array
		{
			return _videosArr;
		}
		
		public function set videosArr(videosArr:Array):void
		{
			_videosArr = videosArr;
			dispatch(new VideoModelEvent(VideoModelEvent.DATA_UPDATED, videosArr));
		}
		
		public function get selectedVideo():VideoVO
		{
			return _selectedVideo;
		}
		
		public function set selectedVideo(video:VideoVO):void
		{
			_selectedVideo = video;
			dispatch(new VideoSelectionChangeEvent(VideoSelectionChangeEvent.VIDEO_SELECTED, video));
		}

		public function get seekToPos():Number
		{
			return _seekToPos;
		}
		
		public function set seekToPos(pos:Number):void
		{
			_seekToPos = pos;
			dispatch(new SeekToPositionEvent(SeekToPositionEvent.SEEK_TO_POS, pos));
		}
	}
}