package models
{
	import flash.display.Stage;
	
	import org.robotlegs.mvcs.Actor;
	
	public class AppModel extends Actor
	{
		private var _stage:Stage;
		private var _dialogType:String;
		
		public function get stage():Stage
		{
			return _stage;
		}
		
		public function set stage(stage:Stage):void
		{
			_stage = stage;
		}

		public function get dialogType():String
		{
			return _dialogType;
		}
		
		public function set dialogType(dialogType:String):void
		{
			_dialogType = dialogType;
		}
	}
}