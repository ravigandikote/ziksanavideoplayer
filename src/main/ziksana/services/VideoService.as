package services
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import models.VideoModel;
	
	import org.robotlegs.mvcs.Actor;
	
	import vo.NoteVO;
	import vo.TagVO;
	import vo.VideoVO;
	
	public class VideoService extends Actor
	{
		public static var instance:VideoService;
		
		[Inject]
		public var videoModel:VideoModel;
		
		public function VideoService()
		{
			
		}
		
		public function fetchVideos():void
		{
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,onLoadComplete);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR,onLoadingError);
			urlLoader.load(new URLRequest("assets/xml/videos.xml"));
		}
		
		private function onLoadComplete(e:Event):void
		{
			XML.ignoreWhitespace = true;
			var data:XML = new XML(e.target.data);
			var videos:XMLList = XMLList(data.video);
			var videosArr:Array = new Array();
			
			for(var i:int=0; i<videos.length(); i++)
			{
				var videoVO:VideoVO = new VideoVO();
				videoVO.title = videos[i].name;
				videoVO.thumbnailSrc = videos[i].thumbnail;
				videoVO.source = videos[i].source;
				
				var tags:XMLList = XMLList(videos[i].tags.tag);
				var tagsArr:Array = new Array();

				var notes:XMLList = XMLList(videos[i].notes.note);
				var notesArr:Array = new Array();
				
				for(var j:int=0; j<tags.length(); j++)
				{
					var tag:TagVO = new TagVO();
					tag.title = tags[j].title;
					tag.description = tags[j].description;
					tag.inTime = tags[j].inTime;
					tag.outTime = tags[j].outTime;
					tag.xpos = tags[j].xpos;
					tag.ypos = tags[j].ypos;
					
					tagsArr.push(tag);
				}
				
				for(var k:int=0; k<notes.length(); k++)
				{
					var note:NoteVO = new NoteVO();
					note.title = notes[k].title;
					note.description = notes[k].description;
					notesArr.push(note);
				}
				
				videoVO.tags = tagsArr;
				videoVO.notes = notesArr;
				videosArr.push(videoVO);
			}
			
			videoModel.videosArr = videosArr;
		}
		
		private function onLoadingError(e:IOErrorEvent):void
		{
			trace("kichcha --> videos loading error?? ",e.errorID);
		}
	}
}