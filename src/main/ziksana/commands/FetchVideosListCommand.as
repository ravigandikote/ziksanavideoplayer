package commands
{
	import org.robotlegs.mvcs.Command;
	
	import services.VideoService;
	
	public class FetchVideosListCommand
	{
		[Inject]
		public var videoService:VideoService;
		
		public function execute():void
		{
			videoService.fetchVideos();
		}
	}
}