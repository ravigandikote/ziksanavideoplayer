package mediators
{
	import events.ControlBtnsClickEvent;
	import events.FullScreenEvent;
	import events.PlaySelectedVideoEvent;
	import events.UpdatePlayheadPositionEvent;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	
	import models.VideoModel;
	
	import org.robotlegs.mvcs.Mediator;
	
	import views.VideoControls;

	public class VideoControlsMediator extends Mediator
	{
		[Inject]
		public var view:VideoControls;
		
		[Inject]
		public var videoModel:VideoModel;
		
		public function VideoControlsMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			view.playArea.addEventListener(MouseEvent.CLICK,onControlsClick);
			view.stopArea.addEventListener(MouseEvent.CLICK,onControlsClick);
			view.seekCanv.addEventListener(MouseEvent.CLICK,onSeekBarClick);
			view.fullScreenBtn.addEventListener(MouseEvent.CLICK,onFullScreen);
			
			eventMap.mapListener(eventDispatcher, PlaySelectedVideoEvent.PLAY_VIDEO,onPlayingSelectedVideo);
			
			eventMap.mapListener(eventDispatcher, UpdatePlayheadPositionEvent.UPDATE_PLAYHEAD_POSITION,updateTime);
		}
		
		private function onSeekBarClick(e:MouseEvent):void
		{
			videoModel.seekToPos = e.target.mouseX;
		}
		
		private function onFullScreen(e:MouseEvent):void
		{
			dispatch(new FullScreenEvent(FullScreenEvent.FULL_SCREEN));
		}
		
		private function updateTime(e:UpdatePlayheadPositionEvent):void
		{
			view.timeTxt.text = e.videoMetaData.timeStr;
			
			view.seekBarHandleSprite.x = (Number(e.videoMetaData.time) /e.videoMetaData.videoDuration)*150;
			view.seekCanv.refresh();
		}
		
		protected function onControlsClick(e:MouseEvent):void
		{
			switch(e.target)
			{
				case view.playArea:
					if(view.playLbl.text == "Play")
					{
						view.playLbl.text = "Pause";
					}
					else
					{
						view.playLbl.text = "Play";
					}
					dispatch(new ControlBtnsClickEvent(ControlBtnsClickEvent.CONTROL_BTNS_CLICK, "playBtn"));
					break;
				
				case view.stopArea:
					dispatch(new ControlBtnsClickEvent(ControlBtnsClickEvent.CONTROL_BTNS_CLICK, "stopBtn"));
					break;
			}
		}
		
		private function onPlayingSelectedVideo(e:PlaySelectedVideoEvent):void
		{
			view.playLbl.text = "Pause";
		}
	}
}