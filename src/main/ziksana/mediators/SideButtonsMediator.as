package mediators
{
	import events.ReadyForAddingTipEvent;
	import events.ShowModalDialogEvent;
	
	import flash.events.MouseEvent;
	import models.AppModel;
	
	import org.robotlegs.mvcs.Mediator;
	
	import views.SideButtonsView;
	
	public class SideButtonsMediator extends Mediator
	{
		[Inject]
		public var view:SideButtonsView;
		
		[Inject]
		public var appModel:AppModel;
		
		public function SideButtonsMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			view.noteBtn.addEventListener(MouseEvent.CLICK,onNoteBtnClick);
			view.tagBtn.addEventListener(MouseEvent.CLICK,onTipBtnClick);
		}
		
		private function onTipBtnClick(e:MouseEvent):void
		{
			appModel.dialogType = ShowModalDialogEvent.SHOW_ADD_TIP_VIEW;
			dispatch(new ReadyForAddingTipEvent(ReadyForAddingTipEvent.READY_FOR_ADDING_TIP, null));
		}
		
		private function onNoteBtnClick(e:MouseEvent):void
		{
			appModel.dialogType = ShowModalDialogEvent.SHOW_NOTE_VIEW;
			dispatch(new ShowModalDialogEvent(ShowModalDialogEvent.SHOW_NOTE_VIEW, null));
		}
	}
}