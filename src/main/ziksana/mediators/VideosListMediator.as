package mediators
{
	import com.soma.ui.layouts.CanvasUI;
	
	import events.PlaySelectedVideoEvent;
	import events.VideoModelEvent;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import models.VideoModel;
	
	import org.osmf.elements.ImageElement;
	import org.robotlegs.mvcs.Mediator;
	
	import views.VideosList;
	
	import vo.VideoVO;

	public class VideosListMediator extends Mediator
	{
		[Inject]
		public var view:VideosList;
		
		[Inject]
		public var videoModel:VideoModel;
		
		private var bm:Bitmap;
		private var videoIndex:int = 0;
		private var videos:Array;
		
		public function VideosListMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			eventMap.mapListener(eventDispatcher, VideoModelEvent.DATA_UPDATED, onDataUpdated);
		}
		
		protected function onDataUpdated(e:VideoModelEvent):void
		{
			videos = e.videos as Array;
			loadImages();
		}
		
		protected function loadImages():void
		{
			var loader:Loader = new Loader();
			loader.load(new URLRequest(videos[videoIndex].thumbnailSrc));
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE,onImageLoadComplete);
		}
		
		protected function onImageLoadComplete(e:Event):void
		{
			bm = e.target.content as Bitmap;
			bm.smoothing = true;
			bm.width = 30;
			bm.height = 30;
			
			var imageHolder:CanvasUI = new CanvasUI(view.videosHolder,30,30);
			imageHolder.addChild(bm);
			view.videosHolder.addChild(imageHolder);
			imageHolder.x = 10;
			imageHolder.y = videoIndex * (imageHolder.height) + 10;
			imageHolder.addEventListener(MouseEvent.CLICK,onThumbnailClick);
			
			if(videoIndex < videos.length-1)
			{
				videoIndex++;
				loadImages();	
			}
		}
		
		protected function onThumbnailClick(e:MouseEvent):void
		{
			var index:int = view.videosHolder.getChildIndex(DisplayObject(e.target));
			var videoVO:VideoVO = videos[index] as VideoVO;
			videoModel.selectedVideo = videoVO;
			dispatch(new PlaySelectedVideoEvent(PlaySelectedVideoEvent.PLAY_VIDEO, videoVO));
		}
	}
}