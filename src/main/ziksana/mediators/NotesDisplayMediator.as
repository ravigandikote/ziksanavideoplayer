package mediators
{
	import com.soma.ui.layouts.VBoxUI;
	
	import events.UpdateNoteEvent;
	import events.VideoSelectionChangeEvent;
	
	import flash.display.Stage;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import org.robotlegs.mvcs.Mediator;
	
	import views.NotesDisplay;
	
	import vo.NoteVO;
	import vo.VideoVO;
	
	public class NotesDisplayMediator extends Mediator
	{
		[Inject]
		public var view:NotesDisplay;
		
		private var noteIndex:int;
		
		public function NotesDisplayMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			eventMap.mapListener(eventDispatcher, VideoSelectionChangeEvent.VIDEO_SELECTED,onVideoSelectionChange);
			eventMap.mapListener(eventDispatcher, UpdateNoteEvent.UPDATE_NOTE,onNoteUpdate);
		}
		
		protected function onNoteUpdate(e:UpdateNoteEvent):void
		{
			noteIndex++;
			renderNotes(e.note);
		}
		
		protected function onVideoSelectionChange(e:VideoSelectionChangeEvent):void
		{
			populateNotes(e.selectedVideo);
		}
		
		protected function populateNotes(video:VideoVO):void
		{
			removeNotes(video);
			for (var i:int = 0; i<video.notes.length; i++)
			{
				noteIndex = i;
				renderNotes(video.notes[i]);
			}
		}
		
		private function removeNotes(video:VideoVO):void
		{
			if(view.notesCanv.numChildren > 0)
			{
				var noteItems:Number = view.notesCanv.numChildren;
				for(var i:int=0; i<noteItems; i++)
				{
					view.notesCanv.removeChildAt(0);					
				}
			}
		}
		
		private function renderNotes(note:NoteVO):void
		{
			var noteElement:VBoxUI = new VBoxUI(view.notesCanv,360,30);
			noteElement.name = "noteElement";
			noteElement.y = noteIndex * 30;
			
			var title:TextField = new TextField();
			title.width = 360;
			title.text = note.title;
			title.x = 10;
			var titleFormat:TextFormat = new TextFormat();
			titleFormat.size = 9;
			titleFormat.color = 0xFF9900;
			title.setTextFormat(titleFormat);
			
			var desc:TextField = new TextField();
			desc.text = note.description;
			desc.width = 360;
			desc.x = 10;
			desc.y = 10;
			var descFormat:TextFormat = new TextFormat();
			descFormat.size = 6;
			descFormat.color = 0xFFFFFF;
			desc.setTextFormat(descFormat);
			
			noteElement.addChild(title);
			noteElement.addChild(desc);
			
			view.notesCanv.addChild(noteElement);
		}
	}
}