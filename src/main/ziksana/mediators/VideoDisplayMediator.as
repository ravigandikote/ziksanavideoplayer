package mediators
{
	import events.ControlBtnsClickEvent;
	import events.FetchVideosListRequest;
	import events.HideModalDialogEvent;
	import events.PlaySelectedVideoEvent;
	import events.ReadyForAddingTipEvent;
	import events.SeekToPositionEvent;
	import events.ShowModalDialogEvent;
	import events.UpdatePlayheadPositionEvent;
	import events.VideoModelEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.net.NetStream;
	import flash.utils.setInterval;
	
	import models.AppModel;
	import models.VideoModel;
	
	import org.robotlegs.mvcs.Mediator;
	
	import views.ModalDialog;
	import views.VideoDisplay;
	
	import vo.TagVO;
	import vo.VideoVO;
	
	public class VideoDisplayMediator extends Mediator
	{
		[Inject]
		public var view:VideoDisplay;
		
		[Inject]
		public var videoModel:VideoModel;
		
		[Inject]
		public var appModel:AppModel;
		
		private var videos:Array;
		
		private var currentTime:Number;
		private var dialog:ModalDialog;
		
		public function VideoDisplayMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			eventMap.mapListener(eventDispatcher, VideoModelEvent.DATA_UPDATED, onDataUpdated);
			eventMap.mapListener(eventDispatcher, PlaySelectedVideoEvent.PLAY_VIDEO,onPlayingSelectedVideo);
			eventMap.mapListener(eventDispatcher, SeekToPositionEvent.SEEK_TO_POS,onSeekingToSomePosition);
			eventMap.mapListener(eventDispatcher, ControlBtnsClickEvent.CONTROL_BTNS_CLICK, onControlsClick);
			eventMap.mapListener(eventDispatcher, ReadyForAddingTipEvent.READY_FOR_ADDING_TIP,onReadyForAddingTip);
			eventMap.mapListener(eventDispatcher, HideModalDialogEvent.HIDE_ADD_TIP_VIEW, hideAddTipView);
			
			view.stream.addEventListener(NetStatusEvent.NET_STATUS,handleStreamStatus);
			
			var time_interval:Number = setInterval(checkTime, 1000, view.stream);
		}
		
		private function onReadyForAddingTip(e:ReadyForAddingTipEvent):void
		{
			view.videoHolder.addEventListener(MouseEvent.CLICK,onVideoDisplayClick);
		}
		
		private function onSeekingToSomePosition(e:SeekToPositionEvent):void
		{
			var exactSeekPos:Number = (Math.round(e.seekToPos) * view.videoDuration)/150;
			view.stream.seek(exactSeekPos);
		}
		
		private function checkTime(ns:NetStream):void 
		{
			var ns_seconds:Number = ns.time;
			currentTime = ns_seconds;
			var minutes:Number = Math.floor(ns_seconds/60);
			var seconds:Object = Math.floor(ns_seconds%60);
			if (seconds<10) {
				seconds = "0"+seconds;
			}
			var videoMetaData:Object = new Object();
			videoMetaData.timeStr = minutes+":"+seconds;
			videoMetaData.time = ns_seconds; 
			videoMetaData.videoDuration = view.videoDuration;
			
			renderTips();
			
			dispatch(new UpdatePlayheadPositionEvent(UpdatePlayheadPositionEvent.UPDATE_PLAYHEAD_POSITION, videoMetaData));
		}
		
		private function renderTips():void
		{
			var tips:Array = videoModel.selectedVideo.tags as Array;
			for(var i:int=0; i<tips.length; i++)
			{
				if(Math.round(currentTime) == tips[i].inTime)
				{
					showTip(TagVO(tips[i]));
				}
				
				if(Math.round(currentTime) == tips[i].outTime)
				{
					hideTip();
				}
			}
		}
		
		private function showTip(tip:TagVO):void
		{
			var tip:TagVO = tip as TagVO;
			var dialog:ModalDialog = new ModalDialog(ShowModalDialogEvent.SHOW_TIP_VIEW,tip);
			dialog.name = "modalDialog";
			dialog.x = Number(tip.xpos);
			dialog.y = Number(tip.ypos);
			view.addChild(dialog);
		}

		private function hideTip():void
		{
			if(view.getChildByName("modalDialog"))
			{
				view.removeChildAt(1);
			}
		}
		
		private function handleStreamStatus(e:NetStatusEvent):void
		{
			trace("kichcha --> event .info. code", e.info.code);
		}
		
		private function onDataUpdated(e:VideoModelEvent):void
		{
			videos = e.videos as Array;
			var video:VideoVO = videos[0] as VideoVO;
			playVideo(video);
			videoModel.selectedVideo = video;
		}
		
		private function playVideo(video:VideoVO):void
		{
			view.stream.close();
			view.videoURL = video.source;
			view.stream.play(view.videoURL);	
			view.isPlaying = true;
		}
		
		private function onPlayingSelectedVideo(e:PlaySelectedVideoEvent):void
		{
			playVideo(e.videoVO);
		}
		
		protected function onVideoDisplayClick(e:MouseEvent):void
		{
			if(appModel.dialogType == ShowModalDialogEvent.SHOW_ADD_TIP_VIEW)
			{
				//view.removeEventListener(MouseEvent.CLICK,onVideoDisplayClick);
				var tip:TagVO = new TagVO();
				tip.xpos = e.target.mouseX;
				tip.ypos = e.target.mouseY;
				tip.title = "New Tip";
				tip.description = "New Tip Description";
				tip.inTime = String(Math.round(currentTime));
				tip.outTime = String(Math.round(currentTime) + 4);
				dialog = new ModalDialog(ShowModalDialogEvent.SHOW_ADD_TIP_VIEW,tip);
				dialog.name = "modalDialog";
				dialog.x = Number(tip.xpos);
				dialog.y = Number(tip.ypos);
				view.addChild(dialog);
			}
		}
		
		private function hideAddTipView(e:HideModalDialogEvent):void
		{
			if(dialog)
			{
				view.removeChild(dialog);
				view.videoHolder.removeEventListener(MouseEvent.CLICK,onVideoDisplayClick);
			}
		}
		
		protected function pauseOrResumeMedia():void
		{
			if(view.isPlaying)
			{
				view.stream.pause();
				view.isPlaying = false;
			}
			else
			{
				view.stream.resume();
				view.isPlaying = true;
			}
		}
		
		protected function onControlsClick(e:ControlBtnsClickEvent):void
		{
			switch(e.itemClicked)
			{
				case "playBtn":
					pauseOrResumeMedia();
					break;
				
				case "stopBtn":
					view.isPlaying = false;
					view.stream.seek(0);
					view.stream.pause();
					break;
			}
		}
	}
}