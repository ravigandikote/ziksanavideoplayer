package mediators
{
	import events.HideModalDialogEvent;
	import events.ReadyForAddingTipEvent;
	import events.ShowModalDialogEvent;
	import events.UpdateNoteEvent;
	import events.VideoSelectionChangeEvent;
	
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import models.VideoModel;
	
	import org.robotlegs.mvcs.Mediator;
	
	import views.ModalDialog;
	
	import vo.NoteVO;
	
	public class ModalDialogMediator extends Mediator
	{
		[Inject]
		public var view:ModalDialog;
		
		[Inject]
		public var videoModel:VideoModel;
		
		public function ModalDialogMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			view.closeBtn.addEventListener(MouseEvent.CLICK,onCloseBtnClick);

			if(view.okBtn)
			{
				view.okBtn.addEventListener(MouseEvent.CLICK,onOkBtnClick);
			}
		}
		
		protected function onCloseBtnClick(e:MouseEvent):void
		{
			if(view.dialogType == ShowModalDialogEvent.SHOW_NOTE_VIEW)
			{
				dispatch(new HideModalDialogEvent(HideModalDialogEvent.HIDE_DIALOG));	
			}
			else if(view.dialogType == ShowModalDialogEvent.SHOW_ADD_TIP_VIEW)
			{
				dispatch(new HideModalDialogEvent(HideModalDialogEvent.HIDE_ADD_TIP_VIEW));
			}
			else
			{
				dispatch(new HideModalDialogEvent(HideModalDialogEvent.HIDE_DYNAMIC_TIP_VIEW));
			}
		}
		
		protected function onOkBtnClick(e:MouseEvent):void
		{
			if(view.titleTxt.text == "" || view.descTxt.text == "")
			{
				var errorTxtFormat:TextFormat = new TextFormat();
				errorTxtFormat.color = 0xFF0000;
				errorTxtFormat.size = 6;
				view.errorTxt.text = "Title or Description is empty";
				view.errorTxt.setTextFormat(errorTxtFormat);
				view.errorTxt.visible = true;
			}
			else
			{
				view.errorTxt.visible = false;
				if(view.dialogType == ShowModalDialogEvent.SHOW_NOTE_VIEW)
				{
					updateNotes();
					dispatch(new HideModalDialogEvent(HideModalDialogEvent.HIDE_DIALOG));
				}
				else
				{
					updateTips();
					dispatch(new HideModalDialogEvent(HideModalDialogEvent.HIDE_ADD_TIP_VIEW));
				}
			}
		}
		
		protected function updateNotes():void
		{
			var note:NoteVO = new NoteVO();
			note.title = view.titleTxt.text;
			note.description = view.descTxt.text;
			
			videoModel.selectedVideo.notes.push(note);
			dispatch(new UpdateNoteEvent(UpdateNoteEvent.UPDATE_NOTE, note));
		}

		protected function updateTips():void
		{
			videoModel.selectedVideo.tags.push(view.tipVO);
			view.tipVO.title = view.titleTxt.text;
			view.tipVO.description = view.descTxt.text;
		}
	}
}