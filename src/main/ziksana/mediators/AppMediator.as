package mediators
{
	import com.soma.ui.layouts.CanvasUI;
	
	import events.FullScreenEvent;
	import events.HideModalDialogEvent;
	import events.ReadyForAddingTipEvent;
	import events.ShowModalDialogEvent;
	
	import flash.display.DisplayObject;
	import flash.display.StageDisplayState;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import models.AppModel;
	
	import org.robotlegs.mvcs.Mediator;
	
	import views.AppView;
	import views.ModalDialog;
	
	import vo.TagVO;
	
	public class AppMediator extends Mediator
	{
		[Inject]
		public var view:AppView;
		
		[Inject]
		public var appModel:AppModel;
		
		public function AppMediator()
		{
			super();
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			eventMap.mapListener(eventDispatcher, ShowModalDialogEvent.SHOW_NOTE_VIEW,onShowingModalDialog);
			eventMap.mapListener(eventDispatcher, HideModalDialogEvent.HIDE_DIALOG,onHidingModalDialog);
			eventMap.mapListener(eventDispatcher, HideModalDialogEvent.HIDE_ADD_TIP_VIEW, hideAddTipView);
			eventMap.mapListener(eventDispatcher, ReadyForAddingTipEvent.READY_FOR_ADDING_TIP,onReadyForAddingTip);
			eventMap.mapListener(eventDispatcher, FullScreenEvent.FULL_SCREEN,onFullScreen);
		}
		
		protected function onShowingModalDialog(e:ShowModalDialogEvent):void
		{
			showDialog(e.type,e.tip);
		}
		
		protected function onReadyForAddingTip(e:ReadyForAddingTipEvent):void
		{
			view.mainCanv.addChildAt(view.transCanv,view.mainCanv.numChildren-2);
			var cancelBtn:CanvasUI = new CanvasUI(view.transCanv,40,15);
			cancelBtn.backgroundAlpha = 1;
			cancelBtn.backgroundColor = 0xFF9878;
			cancelBtn.x = view.videosList.width + view.videoDisplay.width - cancelBtn.width;
			cancelBtn.y = view.videoDisplay.height + 25;
			cancelBtn.mouseChildren = false;
			cancelBtn.buttonMode = cancelBtn.useHandCursor = true;
			
			var cancelLbl:TextField = new TextField();
			cancelLbl.text = "Cancel";
			cancelLbl.width = 40;
			cancelLbl.height = 15;
			cancelBtn.addChild(cancelLbl);
			cancelBtn.addEventListener(MouseEvent.CLICK,onCancelBtnClick);
			view.transCanv.addChildAt(cancelBtn,view.transCanv.numChildren);
		}
		
		private function onCancelBtnClick(e:MouseEvent = null):void
		{
			appModel.dialogType = "";
			view.mainCanv.removeChild(view.transCanv);
		}
		
		public function showDialog(type:String,tip:TagVO):void
		{
			view.mainCanv.addChild(view.transCanv);
			var dialog:ModalDialog = new ModalDialog(type,tip);
			dialog.name = "modalDialog";
			dialog.x = (view.videosList.width+view.videoDisplay.width)/2 - (130/2)
			dialog.y = (view.videoDisplay.height+view.notesDisplay.height)/2 - (110/2);
			view.mainCanv.addChild(view.transCanv);
			view.mainCanv.addChild(dialog);
		}

		protected function onHidingModalDialog(e:HideModalDialogEvent):void
		{
			hideDialog();
		}
		
		public function hideDialog():void
		{
			view.mainCanv.removeChild(view.transCanv);
			if(view.mainCanv.getChildByName("modalDialog"))
			{
				view.mainCanv.removeChildAt(view.mainCanv.numChildren-1);
			}
		}
		
		private function hideAddTipView(e:HideModalDialogEvent):void
		{
			onCancelBtnClick();
		}
		
		private function onFullScreen(e:FullScreenEvent):void
		{
			view._stage.fullScreenSourceRect = new Rectangle(view.videoDisplay.x+60, view.videoDisplay.y, view.videoDisplay.width, view.videoDisplay.height + view.controls.height); 
			view._stage.displayState = StageDisplayState.FULL_SCREEN;
		}
	}
}