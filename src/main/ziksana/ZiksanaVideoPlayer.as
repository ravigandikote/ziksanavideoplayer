package
{
	import com.soma.ui.layouts.CanvasUI;
	import com.soma.ui.layouts.HBoxUI;
	import com.soma.ui.layouts.VBoxUI;
	
	import events.FetchVideosListRequest;
	
	import flash.display.Sprite;
	import flash.media.Video;
	import flash.text.TextField;
	import flash.display.StageScaleMode;
	
	import views.AppView;
	import views.ModalDialog;
	import views.NotesDisplay;
	import views.SideButtonsView;
	import views.VideoControls;
	import views.VideoDisplay;
	import views.VideosList;
	
	public class ZiksanaVideoPlayer extends Sprite
	{
		protected var _context:ZiksanaContext;
		
		public function ZiksanaVideoPlayer()
		{
			trace("kichcha --> Main Constructor bootstrapping RobotLegs");
			_context = new ZiksanaContext(this);
			
			/* Fetch videos at start */
			var fetchVideosEvent:FetchVideosListRequest = new FetchVideosListRequest();
			ZiksanaContext.dispatch(fetchVideosEvent);
			
			var appView:AppView = new AppView(this);
			this.addChild(appView);
			stage.scaleMode = StageScaleMode.EXACT_FIT;
		}
	}
}